package cs.mad.flashcards.entities

data class FlashcardSet(var outTitle:String){
    var title: String = outTitle

    fun createFlashCardSets(): ArrayList<FlashcardSet> {
        var fcardSet = FlashcardSet("default")
        var fcardSetList = arrayListOf<FlashcardSet>()

        for(i in 0..9){
            fcardSet.title = i.toString()

            fcardSetList.add(FlashcardSet(fcardSet.title))
        }

        return fcardSetList
    }
}
