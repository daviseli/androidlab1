package cs.mad.flashcards.entities

data class Flashcard(var outTerm:String, var outDefinition:String){
    var term: String = outTerm
    var definition: String = outDefinition

    fun createFlashcards(): ArrayList<Flashcard> {
        var fcard = Flashcard("default", "default")
        var fcardList = arrayListOf<Flashcard>()

        for(i in 0..9){
            fcard.term = i.toString()
            fcard.definition = i.toString()

            fcardList.add(Flashcard(fcard.term, fcard.definition))
        }

        return fcardList
    }
}
